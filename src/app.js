const express = require("express");
const bodyParser = require("body-parser");

// this catches an exception in a route handler and calls next with it,
// so express' error middleware can deal with it
// saves us a try catch in each route handler
// note: this will be standard in express 5.0, to be released soon
require("express-async-errors");

const app = express();

const helmet = require("helmet");

const morgan = require("morgan");

app.use(bodyParser.json());

app.use(express.json());
// not the topic of this example, but good to be aware of security issues
// helmet sets headers to avoid common security risks
// https://expressjs.com/en/advanced/best-practice-security.html

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.use(helmet());

// use morgan for logging
app.use(morgan("dev"));

const userRoutes = require("./routes/user.routes");
const studentHomeRoutes = require("./routes/studenthome.routes");
const mealRoutes = require("./routes/meal.routes")
const mealOccasionRoutes = require("./routes/mealoccasion.routes")
const authenticationRoutes = require("./routes/authentication.routes")
const InstitutionRoutes = require("./routes/institution.routes")

const errors = require("./errors");

//Main page
app.get("/", (req, res) => {
  let result = {
    message: "Welcome to the StudentEats API!",
  };
  res.status(200);
  res.json(result);
});
app.use(express.json())
app.use("/api", userRoutes)
app.use("/api", studentHomeRoutes)
app.use("/api", mealRoutes)
app.use("/api", mealOccasionRoutes)
app.use("/api", authenticationRoutes)
app.use("/api", InstitutionRoutes)

// catch all not found response
app.use("*", function (_, res) {
  res.status(404).json({
    message: "Endpoint does not exist",
  });
});

// error responses
app.use("*", function (err, req, res, next) {
  console.error(`${err.name}: ${err.message}`);
  // console.error(err)
  next(err);
});

app.use("*", function (err, req, res, next) {
  res.status(500).json({
    message: "Something went wrong",
    error: err.message
  });
});

require("../src/routes/error.routes")(app);

// export the app object for use elsewhere
module.exports = app;
