const User = require("../models/user.model");
const jwt = require("jsonwebtoken");
const config = require("../../authentication.config");
const StudentHome = require("../models/studenthome.model")
const Meal = require("../models/meal.model")
const MealOccasion = require("../models/mealoccasion.model")
const neo4j = require("../../neo");
const Institution = require("../models/institution.model")


module.exports = {
  login(req, res, next) {
    console.log("login called");
    
    
    User.findOne({
      email: req.body.credentials.email,
    })
      .exec((err, user) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        if (!user) {
          return res.status(404).send({ message: "User Not found." });
        }

        var passwordIsValid = (req.body.credentials.password === user.password)

        if (!passwordIsValid) {
          return res.status(401).send({
            message: "Invalid Password!",
          });
        }

        var token = jwt.sign({ id: user._id }, config.jwtSecretKey, {
          expiresIn: 86400, // 24 hours
        });

        user.token = token
        
        user.save()

        res.status(200).send(user);
        return;
      });
  },

  register(req, res, next) {
    console.log("register called");
    const userProps = req.body.user;
    
    
    User.create(userProps)
      .then((user) => {
        // Connect to neo4J
        const session = neo4j.session();
        session
          .run("CREATE (user: User {_id: $_id})", { _id: user._id.toString() })
          .then(() => {
            session.close()
            res.send(user)
          })
          .catch(next);
      })
      .catch(next);

  },

  validateToken(req, res, next) {
    console.log("validateToken called");
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      console.log("Authorization header missing!");
      next({
        message: "Authorization header missing!",
        errCode: 401,
      });
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length);

      jwt.verify(token, config.jwtSecretKey, (err, payload) => {
        if (err) {
          console.log("Not authorized");
          next({
            message: "Not authorized",
            errCode: 401,
          });
        }
        if (payload) {
          console.log("token is valid", payload);
          // User has access, add userid from payload to
          // request, for all following endpoints.
          console.log(payload.id)
          req.userId = payload.id;
          next();
        }
      });
    }
  },

  isStudentHomeAdmin(req, res, next) {
    const userId = req.userId;
    const studenthomeid = req.params.id
    StudentHome.findById(studenthomeid, function(err, studenthome) {
      if (studenthome) {
        console.log("studenthome admin ID = " + studenthome.adminId + " userID is " + userId)
        if(studenthome.adminId == userId){
          console.log("Studenthome admin ID komt overeen met userID")
          next()
        } else {
          console.log("Studenthome admin ID komt niet overeen met userID")
          next({
            message: "You do not have access to this studenthome",
            errCode: 401,
          });
        }
    
      } else if (err) {
        next({
          message: err.message,
          errCode: 500,
        });
      }else{
        next({
          message: `Studenthome with id ${studenthomeid} does not exist`,
          errCode: 404
        })
      }
    })
  },

  isMealAdmin(req, res, next) {
    const userId = req.userId;
    const mealId = req.params.id
    Meal.findById(mealId, function(err, meal) {
      if (meal) {
        if(meal.userId == userId){
          console.log("meal userId komt overeen met userID")
          next()
        } else {
          console.log("meal userId komt niet overeen met userID")
          next({
            message: "You do not have access to this meal",
            errCode: 401,
          });
        }
    
      } else if (err) {
        next({
          message: err.message,
          errCode: 500,
        });
      }else{
        next({
          message: `Meal with id ${mealId} does not exist`,
          errCode: 404
        })
      }
    })
  },
  isMealOccasionAdmin(req, res, next){
    const userId = req.userId;
    const mealoccasionid = req.params.id;
    MealOccasion.findById(mealoccasionid, function(err, mealOccasion){
      if (mealOccasion) {
        if(mealOccasion.userId == userId){
          console.log("meal occasion userId komt overeen met userID")
          next()
        } else {
          console.log("meal occasion userId komt niet overeen met userID")
          next({
            message: "You do not have access to this meal occasion",
            errCode: 401,
          });
        }
    
      } else if (err) {
        next({
          message: err.message,
          errCode: 500,
        });
      }else{
        next({
          message: `Meal occasion with id ${mealoccasionid} does not exist`,
          errCode: 404
        })
      }
    })
  },
  isInstitutionAdmin(req, res, next){
    const userId = req.userId
    const institutionid = req.params.id
    Institution.findById(institutionid)
    .then(doc =>{
      if(doc){
        if(doc.adminId == userId){
          next()
        }else{
          next({
            message: 'You are not the admin of this institution',
            errCode: 401
          })
        }

      }
      else if(err){
        next({message: err.message, errCode: 500})
      }
      else{
        next({
          message: `Institution with id ${institutionid} does not exist`,
          errCode: 404
        })
      }
    })
  }
};