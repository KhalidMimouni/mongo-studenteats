const Institution = require("../models/institution.model");
const { getAll } = require("./user.controller");

module.exports = {
    create(req, res, next){
        console.log('create institution called')
        var institutionprops = req.body.institution;
        institutionprops.adminId = req.userId
        Institution.create(institutionprops)
        .then(doc => res.send(doc))
        .catch(next)
    },

    getAll(req, res, next){
        console.log('Get all institutions called')

        Institution.find({})
        .then(doc => doc.length > 0 ? res.send(doc) : res.send({message: 'No institutions were found in the database'}))
        .catch(next)
    },

    findOneById(req, res, next){
        console.log('Find institution by id called')

        Institution.findById(req.params.id)
        .then(doc => res.send(doc))
        .catch(next)
    },

    updateById(req, res, next){
        Institution.findByIdAndUpdate(req.params.id, req.body.institution)
        .then(doc => doc ? res.send(doc) : res.status(404).send({message: 'Studenthome with given id was not found'}))
        .catch(next)
    },

    deleteById(req, res, next){
        Institution.findByIdAndDelete({_id: req.params.id})
        .then(doc => doc ? res.send(doc) : res.status(404).send({message: 'Institution with given id was not found'}))
        .catch(next)
    }
}