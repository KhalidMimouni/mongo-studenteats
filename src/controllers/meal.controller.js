const Meal = require("../models/meal.model");
const MealOccasion = require("../models/mealoccasion.model")


module.exports = {
  test(req, res, next) {
    console.log("test aangeroepen?");
    res.status(200).json({
      message: "welkom terug",
    });
  },

  create(req, res, next) {
    console.log("create meal called");
    var mealToBeAdded = req.body.meal;
    mealToBeAdded.userId = req.userId;
    console.log(mealToBeAdded);
    
    Meal.create(mealToBeAdded, function(err, meal){
        if(err){
            next({
                message: "Could not create meal at this moment",
            })
        }
        if(meal){
            res.status(200).send(meal)
        }
    })
  },

  edit(req, res, next) {
    console.log("edit meal called");
    const insertedMeal = req.body.meal;
    console.log(insertedMeal);
    Meal.findByIdAndUpdate(req.params.id, insertedMeal, {new: true}, function(err, insertedMeal){
        if(err){
          console.log(err)
            next({
                message: "Could not update meal at this moment"
            })
        }
        if(insertedMeal){
            res.status(200).send({
                message: `Meal with id ${req.params.id} updated succesfully`,
                insertedMeal
            })
        }
    })
    
    
  },

  delete(req, res, next) {
    Meal.findByIdAndDelete(req.params.id)
      .then((meal) => res.status(200).send(meal))
      .catch(next);
  },

  getOne(req, res, next) {
    Meal.findById(req.params.id)
      .then((meal) => res.send(meal))
      .catch(next);
  },

  getAll(req, res, next) {
  
    Meal.find({})
      .then(meals =>{ meals.length > 0 ? res.send(meals) : res.send({message: "No meals were found in the database"})})
      .catch(next);
  },

  getMealOccasions(req, res, next){
    MealOccasion.find({meal: req.params.id})
    .then(mealOccasions => res.send(mealOccasions))
    .catch(next)
  }
};
