const MealOccasion = require("../models/mealoccasion.model");


module.exports = {
  

  create(req, res, next) {
    console.log("create mealoccasion called");
    var mealOccasionToBeAdded = req.body.mealOccasion;

    mealOccasionToBeAdded.userId = req.userId;
    
    console.log(mealOccasionToBeAdded);
    
    MealOccasion.create(mealOccasionToBeAdded, function(err, mealOccasion){
        if(err){
            next({
                message: "Could not create meal occasion at this moment",
            })
        }
        if(mealOccasion){
            res.status(200).send(mealOccasion)
        }
    })
  },
  getAll(req, res, next){
    
    MealOccasion.find()
    .populate(['studentHome', 'meal'])
    .then(mealOccasions =>{ mealOccasions.length > 0 ? res.send(mealOccasions) : res.send({message: "No mealoccasions were found in the database"})})
    .catch(next);
  },

  edit(req, res, next) {
    console.log("edit meal occasion called");
    const insertedMealOccasion = req.body.mealOccasion;
    const options = { new: true };
    console.log(insertedMealOccasion);
    MealOccasion.findByIdAndUpdate(req.params.id, insertedMealOccasion, options, function(err, mealOccasion){
        if(err){
            next({
                message: "Could not update meal at this moment"
            })
        }
        if(mealOccasion){
            MealOccasion.findById(mealOccasion._id).populate(['studentHome', 'meal'])
            .then(updatedMeal => res.send(updatedMeal))
            .catch(next)
        }
    })
    },

  delete(req, res, next) {
    MealOccasion.findByIdAndDelete(req.params.id)
      .then(mealOccasion => res.status(200).send({message: "deleted succesfully", mealOccasion}))
      .catch(next);
  },

  getOne(req, res, next) {
    MealOccasion.findById(req.params.id).populate(['studentHome', 'meal'])
      .then(mealOccasion => res.send(mealOccasion))
      .catch(next);
  },

  applyToMealOccasion(req, res, next){
      const mealId = req.params.id
      MealOccasion.findById(req.params.id, function(err, mealOccasion){
          if(err){
              next({
                  message: "Error. Could not apply to meal occasion."
              })
              
          }
          if(mealOccasion){
            mealOccasion.participantIds.push(req.userId)
            mealOccasion.save()
            res.status(200).send(mealOccasion)
          }else{
              next({
                  message: "Meal occasion with given id does not exist"
              })
          }
      })
  },
  deleteApplication(req, res, next){
    const mealId = req.params.id
    const userId = req.userId
    MealOccasion.findById(req.params.id, function(err, mealOccasion){
        if(err){
            next({
                message: "Error. Could not apply to meal occasion."
            })
            
        }
        if(mealOccasion){
          mealOccasion.participantIds.pull(userId)
          mealOccasion.save()
          res.status(200).send(mealOccasion)
        }else{
            next({
                message: "Meal occasion with given id does not exist"
            })
        }
    })
}
  
};
