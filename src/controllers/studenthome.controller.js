const StudentHome = require("../models/studenthome.model");
const MealOccasion = require("../models/mealoccasion.model")

module.exports = {
  test(req, res, next) {
    console.log("test aangeroepen?");
    res.status(200).json({
      message: "welkom terug",
    });
  },

  create(req, res, next) {
    console.log("create called");
    var studentHomeProps = req.body.studentHome;
    studentHomeProps.adminId = req.userId;
    console.log(studentHomeProps);

    StudentHome.create(studentHomeProps).then((studenthome) => res.send(studenthome));
  },

  edit(req, res, next) {
    const studentHomeProps = req.body.studentHome;
    console.log(studentHomeProps);
    StudentHome.findByIdAndUpdate(req.params.id, studentHomeProps, {new: true})
      .then((studenthome) => res.send(studenthome))
      .catch(next);
  },

  delete(req, res, next) {
    StudentHome.findByIdAndDelete(req.params.id)
      .then((studenthome) => res.status(200).send({message: "Studenthome succesfully deleted", studenthome}))
      .catch(next);
  },

  getOne(req, res, next) {
    StudentHome.findById(req.params.id)
      .then((studenthome) => res.send(studenthome))
      .catch(next);
  },

  getAll(req, res, next) {
    StudentHome.find({})
      .then(studenthomes => studenthomes.length > 0 ? res.send(studenthomes) : res.send({message: 'No studenthomes were found in the database'}))
      .catch(next);
  },
  getAllMealOccasionsForStudentHome(req, res, next){
    MealOccasion.find({studentHome: req.params.id})
    .then(mealOccasions => res.send(mealOccasions))
    .catch(next)
  }
};


