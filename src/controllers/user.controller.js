const User = require("../models/user.model");
const neo4j = require("../../neo");


module.exports = {
  test(req, res, next) {
    console.log("test aangeroepen?");
    res.status(200).json({
      message: "welkom terug",
    });
  },

  create(req, res, next) {
    console.log("create called");
    const userProps = req.body;
    console.log(userProps);
    var session = neo4j.session();

    User.create(userProps)
    .then((user) => 
      {session.run('MERGE (user: User) {id : $userId}) return user.id as userId', {userId: user._id})
      .then(result => {result.records.forEach(record => {console.log(record.get('userId'))})})
      .catch(error => console.log(error))
      .then(session.close())
      res.send(user)}).catch(next)
  },

  edit(req, res, next) {
    User.findOneAndUpdate(req.params.id, req.body, {new: true})
      .then((user) => res.send(user))
      .catch(next);
  },

  delete(req, res, next) {
    User.findOneAndDelete({_id: req.params.id})
      .then(user => user? res.send(user) : next({message: 'User with given id does not exist'}))
      .catch(next);
  },

  getOne(req, res, next) {
    User.findById(req.params.id)
      .then((user) => {res.send(user)})
      .catch(next);
  },

  getAll(req, res, next) {
    User.find({})
      .then(users => res.send(users))
      .catch(err => res.status(500).send({message: err.message}))
  },
  followUser(req, res, next){
    const loggedInUserId = req.userId
    const userToFollowId = req.params.id;
    const session = neo4j.session();
    session.run(`MATCH (loggedInUser : User), (userToFollow : User) 
                  WHERE loggedInUser._id = '${loggedInUserId}' AND userToFollow._id = '${userToFollowId}'
                  CREATE (loggedInUser)-[r:follows]->(userToFollow)
                  RETURN type(r) as r`)
              .then(response => response.records.length? res.send({result: `${loggedInUserId} ${response.records[0].get('r')} ${userToFollowId}`}) 
                                        : next({message: "Empty resultset. user Id does not exist"}))
              .catch(next)
              .then(() => session.close())
                
  },
  unfollowUser(req, res, next){
    const loggedInUserId = req.userId;
    const userToFollowId = req.params.id;
    const session = neo4j.session();
    session.run(`MATCH (a {_id: '${loggedInUserId}'})-[r:follows]->(b {_id: '${userToFollowId}'})
                  DELETE r
                  RETURN *`)
              .then(response => response.records.length? res.send({result: `${loggedInUserId} no longer follows ${userToFollowId}`})
                                        : next({message: "Empty resultset. user Id or relationship does not exist"}))
              .catch(next)
              .then(() => session.close())
  },
  getAllFollowedUsers(req, res, next){
    const loggedInUserId = req.userId;
    
    const session = neo4j.session();
    session.run(`MATCH (a {_id: '${loggedInUserId}'})-[r:follows]-> (users)
                  RETURN users`)
              .then(response => response.records.length? response.records.map(record => record.get('users').properties._id)
                                                       : [])
              .then(userIds => userIds.length? User.find({_id: {$in: userIds}}).then(users => res.send(users)).catch(next)  
                                             : next({message: "Empty resultset. User id does not exist or follows no one"}))
                                              
              .catch(next)
              .then(() => session.close())
                   
  }
};
