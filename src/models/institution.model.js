const mongoose = require('mongoose')
const Schema = mongoose.Schema


const InstitutionSchema = new Schema({
    name:{
        type: String,
        required: [true, 'Institution needs a name']
    },
    address: {
        street:{
            type: String,
            required: [true, 'Studenthome needs a street']
        },
        number:{
            type: Number,
            required: [true, 'Studenthome needs a streetnumber']
        },
        city:{
            type: String,
            required: [true, 'Studenthome needs a city']
        },
        zipcode:{
            type: String,
            required: [true, 'Studenthome needs a zipcode']
        }
    },
    type: {
        type: String,
        required: [true, 'type of instution is not specified']
    },
    adminId: {
        type: Schema.Types.ObjectId,
        required: [true, 'Institution need a user id'],
        ref: 'users'
    }

})

const Institution = mongoose.model('Institution', InstitutionSchema)

module.exports = Institution;