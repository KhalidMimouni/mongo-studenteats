const mongoose = require('mongoose')
const Schema = mongoose.Schema


const MealSchema = new Schema({
    
    userId:{
        type: String,
        required: [true, 'Meal needs id of user']
    },
    name:{
        type: String,
        required: [true, 'Meal needs a name']
    },
    description:{
        type: String,
        required: [true, 'Meal needs a description']
    },
    ingredients:[{
        type: String,
        required: [true, 'Meal needs to have ingredients']
    }],
    allergies:[{
        type: String,
        required: [false]
    }],
    price:{
        type: Number,
        required: [true, 'Meal needs to have a price']
    }
})

const Meal = mongoose.model('Meal', MealSchema)

module.exports = Meal;