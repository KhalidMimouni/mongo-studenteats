const mongoose = require('mongoose')
const Schema = mongoose.Schema


const MealOccasionSchema = new Schema({
    userId:{
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: [true, 'Meal occasion needs to have a userId']
    },
    offeredOn:{
        type: String,
        required: [true, 'Meal needs a date of creation']
    },
    studentHome:{
        type: Schema.Types.ObjectId,
        ref: "StudentHome",
        required: [true, 'Meal occasion needs to have a studentHomeId']
    },
    meal:{
        type: Schema.Types.ObjectId,
        ref: "Meal",
        required: [true, 'Meal occasion needs to have a mealId']
    },
    participantIds:[{
        type: String,
        required: [false]
    }],
    amountOfServings:{
        type: Number,
        required: [true, 'amount of servings  in mealoccasion is required']
    }
})

const MealOccasion = mongoose.model('mealoccasion', MealOccasionSchema)

module.exports = MealOccasion;