const mongoose = require('mongoose')
const Schema = mongoose.Schema



const StudentHomeSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Studenthome needs to have a name.']
    },
    adminId: {
        type: Schema.Types.ObjectId,
        required: [true, 'Studenthome needs administrator Id.']
    },
    address: {
        street:{
            type: String,
            required: [true, 'Studenthome needs a street']
        },
        number:{
            type: Number,
            required: [true, 'Studenthome needs a streetnumber']
        },
        city:{
            type: String,
            required: [true, 'Studenthome needs a city']
        },
        zipcode:{
            type: String,
            required: [true, 'Studenthome needs a zipcode']
        }
    },
    phoneNumber: {
        type: String,
        required: [true, 'A player needs to have a country.']
    },
    createdOn:{
        type: String,
        required: [true, 'Studenthomes needs a date of creation']
    },
    updatedOn:{
        type: String
    }
}, {versionKey: false})

const StudentHome = mongoose.model('StudentHome', StudentHomeSchema)

module.exports = StudentHome;