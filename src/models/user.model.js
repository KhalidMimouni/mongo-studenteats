const mongoose = require('mongoose')
const Schema = mongoose.Schema


const UserSchema = new Schema({
    // a user needs to have a name
    firstname: {
        type: String,
        required: [true, 'A user needs to have a firstname.'],  
    },
    lastname: {
        type: String,
        required: [true, 'A user needs to have a lastname.'],
    },
    email: {
        type: String,
        required: [true, 'A user needs to have an email.'],
        unique: [true, 'A user needs to have an unique email'],
    },
    phonenumber: {
        type: String,
        required: [true, 'A user needs to have a phonenumber.'],
    },
    birthdate: {
        type: Date,
        required: [true, 'A user needs to have a birthdate.'],
    },
    gender: {
        type: String,
        required: [true, 'A user needs to have a gender.']
    },
    institutionId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Institution',
        required: [false]
    },
    token:{
        type: String,
        required: [false]
    },
    password:{
        type: String,
        required: [true, 'A user needs a password']
    }
})

const User = mongoose.model('users', UserSchema)

module.exports = User;