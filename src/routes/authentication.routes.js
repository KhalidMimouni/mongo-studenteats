const express = require('express')
const router = express.Router()


const AuthController = require("../controllers/authentication.controller")

// log in a user
router.post('/login', AuthController.login)

// register a new user
router.post('/register', AuthController.register)

module.exports = router