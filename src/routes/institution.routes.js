const express = require('express')
const router = express.Router()


const InstitutionController = require("../controllers/institution.controller")
const AuthController = require("../controllers/authentication.controller")



// // get all players
router.get('/institutions', InstitutionController.getAll)



// create player
router.post('/institutions', AuthController.validateToken, InstitutionController.create)

// // get a player by id
router.get('/institutions/:id', InstitutionController.findOneById)

// // edit an player
router.put('/institutions/:id', AuthController.validateToken, AuthController.isInstitutionAdmin, InstitutionController.updateById)

// // remove a player
router.delete('/institutions/:id', AuthController.validateToken, AuthController.isInstitutionAdmin, InstitutionController.deleteById)

module.exports = router