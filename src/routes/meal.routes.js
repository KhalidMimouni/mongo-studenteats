const express = require('express')
const router = express.Router()


const MealController = require("../controllers/meal.controller")
const AuthController = require("../controllers/authentication.controller")



// // get all players
router.get('/meals', MealController.getAll)



// create player
router.post('/meals', AuthController.validateToken, MealController.create)

// // get a player by id
router.get('/meals/:id', MealController.getOne)

// // edit an player
router.put('/meals/:id', AuthController.validateToken, AuthController.isMealAdmin, MealController.edit)

// // remove a player
router.delete('/meals/:id', AuthController.validateToken, AuthController.isMealAdmin, MealController.delete)

router.get('meals/:id/mealoccasions', MealController.getMealOccasions)

module.exports = router