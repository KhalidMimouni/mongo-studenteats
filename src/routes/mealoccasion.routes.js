const express = require('express')
const router = express.Router()


const MealOccasionController = require("../controllers/mealoccasion.controller")
const AuthController = require("../controllers/authentication.controller")


// // get all players
router.get('/mealoccasions', MealOccasionController.getAll)




router.post('/mealoccasions', AuthController.validateToken,  MealOccasionController.create)

// // get a player
router.get('/mealoccasions/:id', MealOccasionController.getOne)

// // edit an player
router.put('/mealoccasions/:id', AuthController.validateToken, AuthController.isMealOccasionAdmin, MealOccasionController.edit)

router.post('/mealoccasions/:id', AuthController.validateToken, MealOccasionController.applyToMealOccasion)



router.delete('/mealoccasions/:id', AuthController.validateToken, AuthController.isMealOccasionAdmin, MealOccasionController.delete)

router.delete('/mealoccasions/deleteapplication/:id', AuthController.validateToken, MealOccasionController.deleteApplication)

module.exports = router