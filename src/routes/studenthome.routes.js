const express = require('express')
const router = express.Router()


const StudentHomeController = require("../controllers/studenthome.controller")
const AuthController = require("../controllers/authentication.controller")



// get all players
router.get('/studenthomes', StudentHomeController.getAll)

// test voor Postman :)
router.get('/studenthome', StudentHomeController.test)

router.get('/studenthomes/:id/mealoccasions', StudentHomeController.getAllMealOccasionsForStudentHome)

// create player
router.post('/studenthomes', AuthController.validateToken, StudentHomeController.create)

// get a player
router.get('/studenthomes/:id', StudentHomeController.getOne)

// edit an player
router.put('/studenthomes/:id', AuthController.validateToken, AuthController.isStudentHomeAdmin, StudentHomeController.edit)

// remove a player
router.delete('/studenthomes/:id', AuthController.validateToken, AuthController.isStudentHomeAdmin, StudentHomeController.delete)

module.exports = router