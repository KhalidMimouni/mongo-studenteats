const express = require('express')
const router = express.Router()


const UserController = require("../controllers/user.controller")
const AuthController = require("../controllers/authentication.controller")



// get all users
router.get('/users', UserController.getAll)

// create user
// router.post('/users', UserController.create)

// get a user
router.get('/users/:id', UserController.getOne)

// get a edit
router.put('/users/:id', UserController.edit)

// remove a user
router.delete('/users/:id', UserController.delete)

router.post('/users/follow/:id', AuthController.validateToken, UserController.followUser)


//get followed users
router.get('/users/following/all', AuthController.validateToken, UserController.getAllFollowedUsers)
//delete relationship between users
router.delete('/users/unfollow/:id', AuthController.validateToken, UserController.unfollowUser)

module.exports = router