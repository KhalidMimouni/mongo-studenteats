const assert = require("assert");
const { expect } = require("chai");
const StudentHome = require("../src/models/studenthome.model");
const User = require("../src/models/user.model");


let admin;
describe("Creating studenthome", () =>{
    
    

    before(done =>{
        var user = {
        firstname: "Khalid",
        lastname: "Mimouni",
        password: "12345",
        email: "khalid@hotmail.com",
        phonenumber: "06557777",
        birthdate: "2020-01-01",
        gender: "Male",
        token: "",
        };
        admin = new User(user)
        admin.save().then(() => done()).catch(done)
    })
    it("should add created studenthome to database", done =>{
        let studenthome;
        var newStudentHome = {
            _id: "61b2c55b42015955ccf4e781",
            name: "Avans",
            adminId: admin._id,
            address:{
                street: "Lovensdijkstraat",
                number: 63,
                city: "Breda",
                zipcode: "4813PA"
            },
            phoneNumber: "0639308067",
            createdOn: "2020-10-10",
            updatedOn: "2020-10-10"
        }
        studenthome = new StudentHome(newStudentHome);
        studenthome.save().then(result => {
            
            assert.equal(result._id, newStudentHome._id, 'Studenthomes are not equal')
            done()
        })
        .catch(done)
        
    })
})
describe("Getting studenthome by id", () =>{
    
    it("Should return studenthome by given id", done =>{
        
        StudentHome.findById("61b2c55b42015955ccf4e781")
        .then(result => {
            assert.equal("61b2c55b42015955ccf4e781", result._id, 'Ids are not equal')
            done()
        })
        .catch(done)
        
    })
})
describe("Update studenthome by id", () =>{
    
    it("Should return with given id and updated values", done =>{
        var studenthome = {
            _id: "61b2c55b42015955ccf4e781",
            name: "Fontys",
            adminId: admin._id,
            address:{
                street: "Lovensdijkstraat",
                number: 63,
                city: "Breda",
                zipcode: "4813PA"
            },
            phoneNumber: "0639308067",
            createdOn: "2020-10-10",
            updatedOn: "2020-10-10"
        }
        
        StudentHome.findByIdAndUpdate("61b2c55b42015955ccf4e781", studenthome, {new: true})
        .then(result => {
            assert.equal("61b2c55b42015955ccf4e781", result._id, 'Ids are not equal')
            assert.equal(result.name, 'Fontys', 'Update did not happen successfully')
            done()
        })
        .catch(done)
        
    })
})
describe("Getting studenthome by id", () =>{
    
    it("Should return studenthome by given id", done =>{
        
        StudentHome.findById("61b2c55b42015955ccf4e781")
        .then(result => {
            assert.equal("61b2c55b42015955ccf4e781", result._id, 'Ids are not equal')
            done()
        })
        .catch(done)
        
    })
})

describe("Get studenthomes", () =>{
    
    it("Should return a list of 2 studenthomes", done =>{
        var studenthome = {
            _id: "61b2c55b42015955ccf4e782",
            name: "Fontys",
            adminId: admin._id,
            address:{
                street: "Lovensdijkstraat",
                number: 63,
                city: "Breda",
                zipcode: "4813PA"
            },
            phoneNumber: "0639308067",
            createdOn: "2020-10-10",
            updatedOn: "2020-10-10"
        }
        
        StudentHome.create(studenthome)
        .then(() => StudentHome.find({}).then(results => {
            expect(results.length).to.equal(4) 
            done()}).catch(done))
        .catch(done)
        
    })
})
// describe("Delete studenthome", ()=>{
//     it("Should delete a studenthome by given id", done =>{
        
//     })
// })