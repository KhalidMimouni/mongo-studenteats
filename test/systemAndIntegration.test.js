require('dotenv').config()
process.env.MONGO_PROD_DB = process.env.MONGO_TEST_DB

const chai = require("chai")
const chaiHttp = require("chai-http")
const jwt = require('jsonwebtoken')
const assert = require("assert")
const server = require("../server")
const StudentHome = require("../src/models/studenthome.model")
const User = require("../src/models/user.model")
const MealOccasions = require("../src/models/mealoccasion.model")
const Meal = require("../src/models/meal.model")
const neo4j = require('../neo')


chai.should()
chai.use(chaiHttp)

before(done => {
    var studenthome1 = {
        _id: "61b2c55b42015955ccf4e790",
        name: "Fontys",
        adminId: "61b2c55b42015955ccf4e800",
        address:{
            street: "Lovensdijkstraat",
            number: 63,
            city: "Breda",
            zipcode: "4813PA"
        },
        phoneNumber: "0639308067",
        createdOn: "2020-10-10",
        updatedOn: "2020-10-10"
    }

    var studenthome2 = {
        _id: "61b2c55b42015955ccf4e590",
        name: "Avans",
        adminId: "61b2c55b42015955ccf4e800",
        address: { 
            street: "Vloed",
            number: 9,
            city: "Breda",
            zipcode: "5000PA"
        },
        phoneNumber: "0639308067",
        createdOn: "2020-10-10",
        updatedOn: "2020-10-10"
    }
    var user = {
        _id: "61b2c55b42015955ccf4e800",
        firstname: "Khalid",
        lastname: "Mimouni",
        email: "khalid@gmail.com",
        phonenumber: "0639308067",
        birthdate: "2020-10-10",
        gender: "Male",
        token: "",
        password: "Khalid123"

    }
    var user2 = {
        _id: "61b2c55b42015955ccf4e555",
        firstname: "Hans",
        lastname: "Mimouni",
        email: "khalid123@gmail.com",
        phonenumber: "0639308067",
        birthdate: "2020-10-10",
        gender: "Male",
        token: "",
        password: "Khalid123"

    }

    var mealOccasion = {
        _id: "61b2c55b42015955ccf4e300",
        userId: "61b2c55b42015955ccf4e800",
        offeredOn: "2020-10-10",
        studentHome: "61b2c55b42015955ccf4e790",
        participantIds: [],
        amountOfServings: 30,
        meal: "61b2c55b42015955ccf4e111"
    }

    var meal = {
        _id: "61b2c55b42015955ccf4e111",
        userId: "61b2c55b42015955ccf4e800",
        name: "hutspot",
        description: "beschrijving",
        ingredients: ["iets"],
        allergies: ["niks"],
        price: 5.50
    }
    
    StudentHome.insertMany([studenthome1, studenthome2])
    .then(() => User.insertMany([user, user2])
    .then(() => Meal.create(meal)
    .then(() => MealOccasions.create(mealOccasion)
    .then(() => {
        const session = neo4j.session()
        session
        .run(`CREATE (user1: User {_id : '${user._id}'}), (user2: User {_id : '${user2._id}' })`)
        .then(() => {
            session.close()
            done()
        })
    }))))
    .catch(done)
})

after(done =>{
    StudentHome.deleteMany({})
    .then(() => User.deleteMany({})
    .then(() => Meal.deleteMany({})
    .then(() => MealOccasions.deleteMany({})
    .then(() => {
        const session = neo4j.session()
        session
        .run(`MATCH (user1: User {_id : '61b2c55b42015955ccf4e555'}), (user2: User {_id : '61b2c55b42015955ccf4e800' }) DELETE user1, user2`)
        .then(() => {
            session.close()
            done()
        }) 
    }))))
    .catch(done)
})

describe("Studenthomes", function(){
    describe("GET/STUDENTHOMES -->" ,function(){
        it("should retrieve all studenthomes", done =>{
            chai
                .request(server)
                .get("/api/studenthomes")
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(200)
                    res.body.should.be.an("array")
                    done()
                })
        })
    })
    describe("GET/STUDENTHOME/:ID -->", function(){
        it("should retrieve studenthome by given id", done =>{
            chai
            .request(server)
            .get("/api/studenthomes/61b2c55b42015955ccf4e790")
            .end((err, res) => {
                assert.ifError(err)
                res.should.have.status(200)
                res.body.should.be.an("object")
                
                done()
            })
        })
    })
    describe("PUT/STUDENTHOMES/:ID -->", function(){
        
        it("should edit and return studenthome by given id", done =>{
            jwt.sign({id: "61b2c55b42015955ccf4e800"}, 'secret-token', { expiresIn: '2h'}, (err, token) =>{
                chai
                .request(server)
                .put("/api/studenthomes/61b2c55b42015955ccf4e790")
                .set('authorization', 'Bearer ' + token)
                .send({
                    studentHome: {
                        _id: "61b2c55b42015955ccf4e790",
                        name: "Avans",
                        adminId: "61b2c55b42015955ccf4e791",
                        address:{
                            street: "Lovensdijkstraat",
                            number: 63,
                            city: "Breda",
                            zipcode: "4813PA"
                        },
                        phoneNumber: "0639308067",
                        createdOn: "2020-10-10",
                        updatedOn: "2020-10-10"
                    }
                })
                .end((err, res) => {
                    assert.ifError(err)
                    res.should.have.status(200)
                    res.body.should.be.an("object")
                    assert.equal(res.body.name, "Avans", 'Names do not match')
                    done()
                })
            })
            
        })
    })
    describe("DELETE/STUDENTHOMES/:ID -->", function(){
        it("should return the studenthome by given id", done =>{
            jwt.sign({id: "61b2c55b42015955ccf4e800"}, 'secret-token', {expiresIn: '2h'}, (err, token) =>{
                chai
                .request(server)
                .delete('/api/studenthomes/61b2c55b42015955ccf4e590')
                .set('authorization', 'Bearer ' + token)
                .end((err, res) => {
                    assert.ifError(err)
                    res.should.have.status(200)
                    done()
                })
            })
        })
    })
    describe("POST/STUDENTHOMES -->", function(){
        it("should create studenthome and return it in body", done =>{
            jwt.sign({id: "61b2c55b42015955ccf4e800"}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .post('/api/studenthomes')
                .set('authorization', 'Bearer ' + token)
                .send({
                    studentHome:{
                        name: "TU Delt",
                        address: { 
                            street: "Vloed",
                            number: 9,
                            city: "Breda",
                            zipcode: "5000PA"
                        },
                        phoneNumber: "0639308067",
                        createdOn: "2020-10-10",
                        updatedOn: "2020-10-10"
                    }
                })
                .end((err, res) => {
                    assert.ifError(err)
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    assert.equal("61b2c55b42015955ccf4e800", res.body.adminId, 'userId is not equal to created studenthome adminId')
                    done()
                })
            })
        })
    })
    describe("GET/STUDENTHOMES/:ID/MEALOCCASIONS -->", function(){
        it("should return all mealoccasions for studenthome with given id", done => {
            chai
            .request(server)
            .get('/api/studenthomes/61b2c55b42015955ccf4e790/mealoccasions')
            .end((err, res) => {
                assert.ifError(err)
                res.should.have.status(200)
                res.body.should.be.an('array')
                assert.equal(res.body[0].studentHome, "61b2c55b42015955ccf4e790", 'Retrieved mealoccasion does not match studenthome' )
                done()
            })
        })
    })
})

describe("Users", function(){
    describe("GET/USERS/:ID -->", function(){
        it("should return all users", done => {
            chai
            .request(server)
            .get('/api/users/61b2c55b42015955ccf4e800')
            .end((err, res) => {
                assert.ifError(err)
                res.should.have.status(200)
                res.body.should.be.an('object')
                assert.equal(res.body._id, "61b2c55b42015955ccf4e800", 'Expected ID does not match id of user in database')
                done()
            })
        })
    })
    
    describe("USERS/FOLLOW/:ID -->", function(){
        it("should return a message if one of the user ids do not exist", done =>{
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .post('/api/users/follow/61b2c55b42015955ccf4e554')
                .set('authorization', 'Bearer ' + token)
                .end((err, res) => {
                    
                    res.body.should.be.an('object')
                    assert.equal(res.body.error, 'Empty resultset. user Id does not exist', 'expected message doesnt match outcome')
                    done()
                })
            })
        })
        it("should return a confirmation message if user is succesfully followed", done =>{
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .post('/api/users/follow/61b2c55b42015955ccf4e555')
                .set('authorization', 'Bearer ' + token)
                .end((err, res) => {
                    
                    res.body.should.be.an('object')
                    assert.equal(res.body.result, '61b2c55b42015955ccf4e800 follows 61b2c55b42015955ccf4e555', 'expected message doesnt match outcome')
                    done()
                })
            })
        })
    })
    describe("USERS/FOLLOWING/ALL -->", function(){
        it('should return appropriate message if user does not follow anyone or id does not exist', done =>{
            jwt.sign({id: '61b2c55b42015955ccf4e801'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .get('/api/users/following/all')
                .set('authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.body.should.be.an('object')
                    assert.equal(res.body.error, 'Empty resultset. User id does not exist or follows no one', 'expected message doesnt match outcome')
                    done()
                })
            })
        })
        it('should return a list of followed users', done =>{
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .get('/api/users/following/all')
                .set('authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.body.should.be.an('array')
                    assert.equal(res.body[0]._id, '61b2c55b42015955ccf4e555', 'Ids do not match')
                    done()
                })
            })
        })
    })
    describe("USERS/UNFOLLOW/:ID -->", function(){
        it('should give appropriate message if relationship or one of the user ids does not exist', done => {
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .delete('/api/users/unfollow/61b2c55b42015955ccf4e553')
                .set('authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.body.should.be.an('object')
                    assert.equal(res.body.error, 'Empty resultset. user Id or relationship does not exist', 'Messages do not match')
                    done()
                })
            })
        })
        it('should return confirmation when relationship has succesfully been deleted', done => {
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .delete('/api/users/unfollow/61b2c55b42015955ccf4e555')
                .set('authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.body.should.be.an('object')
                    assert.equal(res.body.result, '61b2c55b42015955ccf4e800 no longer follows 61b2c55b42015955ccf4e555', 'Messages do not match')
                    done()
                })
            })
        })
    })
    describe("PUT/USERS/:ID -->", function(){
        it("should update user in database and return it", done =>{
            jwt.sign({id: "61b2c55b42015955ccf4e800"}, 'secret-token', {expiresIn: '2h'}, (err, token) => { 
                chai
                .request(server)
                .put('/api/users/61b2c55b42015955ccf4e800')
                .set('authorization', 'Bearer ' + token)
                .send({
                    _id: "61b2c55b42015955ccf4e800",
                    firstname: "Khalid",
                    lastname: "Mimouni",
                    email: "khalidmimouni@gmail.com",
                    phonenumber: "0639308067",
                    birthdate: "2020-10-10",
                    gender: "Male",
                    token: "",
                    password: "Khalid123"
            
                })
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(200)
                    res.should.be.an('object')
                    
                    assert.equal(res.body._id, '61b2c55b42015955ccf4e800', 'Id of updated user does not match input')
                    done()
                })
            })
        })

    })
    describe("DELETE/USER/:ID -->", function(){
        it("should delete user with given id and return it", done => {
                chai
                .request(server)
                .delete('/api/users/61b2c55b42015955ccf4e555')
                .end((err, res) => {
                    assert.ifError(err)
                    res.should.have.status(200)
                    res.should.be.an('object')
                    assert.equal(res.body.firstname, 'Hans', 'Name of deleted user does not match name of user with given id')
                    done()
                })
            
        })
    })
})
describe("Meals", function(){
    describe("GET/MEALS -->", function(){
        it("should return a list of all meals", done =>{
            chai
            .request(server)
            .get('/api/meals')
            .end((err, res) =>{
                assert.ifError(err)
                res.should.have.status(200)
                res.body.should.be.an('array')
                assert.equal(res.body[0]._id, "61b2c55b42015955ccf4e111", 'Ids do not match')
                done()
            })
        })
        
        it("should return a message if no meals were found in database", done =>{
            Meal.deleteMany({}).then(() =>{
                chai
                .request(server)
                .get('/api/meals')
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    assert.equal(res.body.message, "No meals were found in the database", 'Messages do not match')
                    done()
                })
            })
            .catch(done)
        
        })
        
        
    })
    describe("POST/MEALS -->", function(){
        it("should create a meal and return it", done =>{
            jwt.sign({id: "61b2c55b42015955ccf4e800"}, 'secret-token', {expiresIn: '2h'}, (err, token) =>{
                chai
                .request(server)
                .post('/api/meals')
                .set('authorization', 'Bearer ' + token)
                .send({
                    meal: {
                        _id: "61b2c55b42015955ccf4e111",
                        name: "spaghetti",
                        description: "beschrijving",
                        ingredients: ["iets"],
                        allergies: ["niks"],
                        price: 5.50
                    }
                })
                .end((err, res) => {
                    assert.ifError(err)
                    res.body.should.be.an('object')
                    assert.equal(res.body.name, 'spaghetti', 'Meal names do not match')
                    assert.equal(res.body.userId, '61b2c55b42015955ccf4e800', 'user ids do not match' )
                    done()
                })
            })
        })
        it("return an error message if not logged in", done =>{
            jwt.sign({id: "61b2c55b42015955ccf4e800"}, 'secret-token', {expiresIn: '2h'}, (err, token) =>{
                chai
                .request(server)
                .post('/api/meals')
                .send({
                    meal: {
                        _id: "61b2c55b42015955ccf4e111",
                        name: "spaghetti",
                        description: "beschrijving",
                        ingredients: ["iets"],
                        allergies: ["niks"],
                        price: 5.50
                    }
                })
                .end((err, res) => {
                    assert.ifError(err)
                    res.body.should.be.an('object')
                    assert.equal(res.body.error, 'Authorization header missing!', 'error messages do not match' )
                    done()
                })
            })
        })
    })
    describe("GET/MEAL/:ID -->", function(){
        it("should return a meal that matches the given id", done =>{
            chai
            .request(server)
            .get('/api/meals/61b2c55b42015955ccf4e111')
            .end((err, res) => {
                assert.ifError(err)
                res.body.should.be.an('object')
                assert.equal(res.body._id, "61b2c55b42015955ccf4e111", 'The input id does not match the meal id')
                done()
            })
        })
    })
    describe("PUT/MEAL/:ID -->", function(){
        jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token)=>{
            it("should update meal and return it", done => {
                chai
                .request(server)
                .put('/api/meals/61b2c55b42015955ccf4e111')
                .set('authorization', 'Bearer ' + token)
                .send({
                    meal: {
                        userId: "61b2c55b42015955ccf4e800",
                        name: "hutspot",
                        description: "beschrijving",
                        ingredients: ["iets"],
                        allergies: ["niks"],
                        price: 5.50
                    }
                })
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(200)
                    res.should.be.an('object')
                    console.log(res.body)
                    assert.equal(res.body.insertedMeal._id, '61b2c55b42015955ccf4e111', 'Ids do not match')
                    assert.equal(res.body.insertedMeal.name, 'hutspot', 'names do not match')
                    done()
                })
            })
        })
        jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token)=>{
            it('should display not authorized message if user does not provide valid token', done => {
               chai
               .request(server) 
               .put('/api/meals/61b2c55b42015955ccf4e111')
               .set('authorization', 'Bearer ' + 'onzinnigetoken')
               .send({
                    meal: {
                        userId: "61b2c55b42015955ccf4e800",
                        name: "hutspot",
                        description: "beschrijving",
                        ingredients: ["iets"],
                        allergies: ["niks"],
                        price: 5.50
                    }
                })
                .end((err, res) => {
                    assert.ifError(err)
                    res.should.be.an('object')
                    assert.equal(res.body.error, 'Not authorized')
                    done()
                })
            })
        })
        jwt.sign({id: '61b2c55b42015955ccf4e801'}, 'secret-token', {expiresIn: '2h'}, (err, token)=>{
            it('should notify user that they are not admin if trying to update meal that they dont own', done => {
               chai
               .request(server) 
               .put('/api/meals/61b2c55b42015955ccf4e111')
               .set('authorization', 'Bearer ' + token)
               .send({
                    meal: {
                        userId: "61b2c55b42015955ccf4e800",
                        name: "hutspot",
                        description: "beschrijving",
                        ingredients: ["iets"],
                        allergies: ["niks"],
                        price: 5.50
                    }
                })
                .end((err, res) => {
                    assert.ifError(err)
                    res.should.be.an('object')
                    assert.equal(res.body.error, 'You do not have access to this meal')
                    done()
                })
            })
        })
    })
    describe("DELETE/MEAL/:ID -->", function(){
        
            
                it('should notify user that they are not admin if trying to delete meal that they dont own', done => {
                    jwt.sign({id: '61b2c55b42015955ccf4e801'}, 'secret-token', {expiresIn: '2h'}, (err, token)=>{
                        chai
                        .request(server) 
                        .delete('/api/meals/61b2c55b42015955ccf4e111')
                        .set('authorization', 'Bearer ' + token)
                        .end((err, res) => {
                            assert.ifError(err)
                            res.should.be.an('object')
                            assert.equal(res.body.error, 'You do not have access to this meal')
                            done()
                        })
                    })
                   
                })
           
        
            
                it('should notify user if they didnt provide  a valid token', done => {
                    jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token)=>{
                        chai
                        .request(server) 
                        .delete('/api/meals/61b2c55b42015955ccf4e111')
                        .set('authorization', 'Bearer ' + 'onzinnigetoken')
                            .end((err, res) => {
                                assert.ifError(err)
                                res.should.be.an('object')
                                assert.equal(res.body.error, 'Not authorized')
                                done()
                            })
                    })
                })
            
                it('should delete meal and return it', done => {
                    jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token)=>{
                        chai
                        .request(server) 
                        .delete('/api/meals/61b2c55b42015955ccf4e111')
                        .set('authorization', 'Bearer ' + token)
                            .end((err, res) => {
                                assert.ifError(err)
                                res.should.be.an('object')
                                assert.equal(res.body._id, '61b2c55b42015955ccf4e111', 'ids do not match')
                                done()
                        })
                })
            })


    })
})
describe('Meal occasions', function(){
    describe('GET/MEALOCCASIONS -->', function(){
        it("should return a list of all mealoccasions", done => {
            chai
            .request(server)
            .get('/api/mealoccasions')
            .end((err, res) => {
                assert.ifError(err)
                res.should.have.status(200)
                res.body.should.be.an('array')
                res.body.should.have.length(1)
                done()
            })
        })
        it("should return a message if no mealoccasions were found in database", done => {
            MealOccasions.deleteMany({})
            .then(() => {
                chai
                .request(server)
                .get('/api/mealoccasions')
                .end((err, res) => {
                    assert.ifError(err)
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    assert.equal('No mealoccasions were found in the database', res.body.message, 'Message not equal')
                    done()
                })
            })
            .catch(done)
            
        })
    })
    describe("POST/MEALOCCASIONS -->", function(){
        it("should add mealoccasion to database and return it", done => {
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .post('/api/mealoccasions')
                .set('authorization', 'Bearer ' + token)
                .send({
                    mealOccasion: {
                        _id: '61b2c55b42015955ccf4e753',
                        offeredOn: "2020-10-10",
                        studentHome: "61b2c55b42015955ccf4e790",
                        meal: "61b2c55b42015955ccf4e111",
                        participantsId: [],
                        amountOfServings: 30
                    }
                })
                .end((err, res) =>{
                    assert.ifError(err)
                    console.log(res.body)
                    res.should.have.status(200)
                    assert.equal('61b2c55b42015955ccf4e753', res.body._id, 'Ids are not equal')
                    done()
                })
            })
            
        })
        it("should give error message if token is not valid", done => {
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .post('/api/mealoccasions')
                .set('authorization', 'Bearer ' + 'invalidtoken')
                .send({
                    mealOccasion:{
                        _id: '61b2c55b42015955ccf4e753',
                        offeredOn: "2020-10-10",
                        studentHome: "61b2c55b42015955ccf4e790",
                        meal: "61b2c55b42015955ccf4e111",
                        participantsId: [],
                        amountOfServings: 30
                    }
                })
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(500)
                    assert.equal('Not authorized', res.body.error, 'Ids are not equal')
                    done()
                })
            })
        })
    })
    describe('PUT/MEALOCCASIONS/:ID', function(){
        it('should give error if token is invalid', done => {
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .put('/api/mealoccasions/61b2c55b42015955ccf4e753')
                .set('authorization', 'Bearer ' + 'invalidtoken')
                .send({
                    mealOccasion:{
                        _id: '61b2c55b42015955ccf4e753',
                        offeredOn: "2020-10-10",
                        studentHome: "61b2c55b42015955ccf4e790",
                        meal: "61b2c55b42015955ccf4e111",
                        participantsId: [],
                        amountOfServings: 45
                    }
                })
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(500)
                    assert.equal('Not authorized', res.body.error, 'Ids are not equal')
                    done()
                })
            })
        })
        it('should give error if user is not admin', done => {
            jwt.sign({id: '61b2c55b42015955ccf4e801'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .put('/api/mealoccasions/61b2c55b42015955ccf4e753')
                .set('authorization', 'Bearer ' + token)
                .send({
                    mealOccasion:{
                        _id: '61b2c55b42015955ccf4e753',
                        offeredOn: "2020-10-10",
                        studentHome: "61b2c55b42015955ccf4e790",
                        meal: "61b2c55b42015955ccf4e111",
                        participantsId: [],
                        amountOfServings: 45
                    }
                })
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(500)
                    assert.equal('You do not have access to this meal occasion', res.body.error, 'Messages are not equal')
                    done()
                })
            })
        })
        it('should update mealoccasion and return it', done => {
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .put('/api/mealoccasions/61b2c55b42015955ccf4e753')
                .set('authorization', 'Bearer ' + token)
                .send({
                    mealOccasion:{
                        _id: '61b2c55b42015955ccf4e753',
                        offeredOn: "2020-10-10",
                        studentHome: "61b2c55b42015955ccf4e790",
                        meal: "61b2c55b42015955ccf4e111",
                        participantsId: [],
                        amountOfServings: 45
                    }
                })
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(200)
                    assert.equal(45, res.body.amountOfServings, 'Updated field didnt have expected value')
                    done()
                })
            })
        })
    })
    describe('DELETE/MEALOCCASIONS/:ID', function(){
        it('should give error if token is invalid', done => {
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .delete('/api/mealoccasions/61b2c55b42015955ccf4e753')
                .set('authorization', 'Bearer ' + 'invalidtoken')
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(500)
                    assert.equal('Not authorized', res.body.error, 'Ids are not equal')
                    done()
                })
            })
        })
        it('should give error if user is not admin of mealoccasion', done => {
            jwt.sign({id: '61b2c55b42015955ccf4e801'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .delete('/api/mealoccasions/61b2c55b42015955ccf4e753')
                .set('authorization', 'Bearer ' + token)
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(500)
                    assert.equal('You do not have access to this meal occasion', res.body.error, 'messages are not equal')
                    done()
                })
            })
        })
        it('should delete mealoccasion and return it', done => {
            jwt.sign({id: '61b2c55b42015955ccf4e800'}, 'secret-token', {expiresIn: '2h'}, (err, token) => {
                chai
                .request(server)
                .delete('/api/mealoccasions/61b2c55b42015955ccf4e753')
                .set('authorization', 'Bearer ' + token)
                .end((err, res) =>{
                    assert.ifError(err)
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    assert.equal('61b2c55b42015955ccf4e753', res.body.mealOccasion._id, 'Ids are not equal')
                    done()
                })
            })
        })
    })

})


