const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
const InstitutionModel = require("../src/models/institution.model")

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
}

before((done) => {
  const Institution = {
    name: "Avans",
    address:{
      street: "Lovensdijkstraat",
      number: 63,
      city: "Breda",
      zipcode: "4813PA"
    },
    type: "Hogeschool"
  }
  mongoose.connect('mongodb://localhost/StudentEatsTest', options);
  mongoose.connection
    .once("open", () => {
      InstitutionModel.create(Institution)
      .then(() => done())
      .catch(done)
    })
    .on("error", done);
});

after((done) => {
  const { users, meals, studenthomes, mealoccasions, institutions} = mongoose.connection.collections;
  
  institutions.deleteMany({}, ()=>{
    users.deleteMany({}, () => {
      studenthomes.deleteMany({}, ()=> {
        done()
      })
    })
  })
});
