const assert = require("assert");
const User = require("../src/models/user.model");

describe("Creating User", () => {
  it("Saves a user", (done) => {
    const khalid = {
      firstname: "Khalid",
      lastname: "Mimouni",
      password: "1234556",
      email: "khalid@mail.nl",
      phonenumber: "06557777",
      birthdate: "2020-01-01",
      gender: "Male",
      token: "",
    }
    User.create(khalid)
    .then(() => done())
    .catch(done)
    });
});


describe("Reading user", () => {
  let user;
  before((done) => {
    user = {
      firstname: "Jan",
      lastname: "Achternaam",
      password: "1234556",
      email: "jan12@mail.nl",
      phonenumber: "06557777",
      birthdate: "2020-01-01",
      gender: "Other",
      token: ""
    }
    User.create(user)
    .then(() => done())
    .catch(done)
  });

  it("Reads a user", (done) => {
    User.findOne({ firstname: "Jan" }).then((user) => {
      assert(user._id.toString() == user._id.toString(), done())
      }).catch(done)
});

describe("Deleting user", () => {
  let user;
  beforeEach((done) => {
    user = User({
      firstname: "Freek",
      lastname: "Vonk",
      password: "1234556",
      email: "vonk@mail.nl",
      phonenumber: "06557777",
      birthdate: "2020-01-01 10:10:00",
      gender: "Other",
    })
    user.save()
    .then(() => done())
    .catch(done)
  });

  it("Deletes a user", (done) => {
    User.findByIdAndDelete(user._id).then(() => {
      User.findOne({ firstname: "Freek" }).then((user) => {
        assert(user === null);
        done();
      })
      .catch(done)
    });
  });
});

describe("Updating user", () => {
  let user;
  beforeEach((done) => {
    user = new User({
      firstname: "janalleman",
      lastname: "Achternaam",
      password: "1234556",
      email: "Jan@mail.nl",
      phonenumber: "06557777",
      birthdate: "2020-01-01 10:10:00",
      gender: "Other",
    });
    user.save()
    .then(() => done)
    .catch(done)
    });
  });

  it("Set email on user", (done) => {
    User.updateOne({ firstname: "janalleman" }, { email: "niewemail@mail.com" }).then(
      () => {
        User.findOne({ firstname: "janalleman" }).then((user) => {
          done();
        })
        .catch(done)
      }
    );
  });
});

describe("User validation", () => {
  it("User needs a name", (done) => {
    const user = new User({
      firstname: undefined,
      lastname: "Achternaam",
      password: "1234556",
      email: "Jan@mail.nl",
      phonenumber: "06557777",
      birthdate: "2020-01-01 10:10:00",
      gender: "Other",
    });
    const validationResult = user.validateSync();
    const { message } = validationResult.errors.firstname;
    assert(message == "A user needs to have a firstname.");
    done();
  });
  it("Faulty user cant be saved on the database", (done) => {
    const user = new User({
      firstname: undefined,
      lastname: "Achternaam",
      password: "1234556",
      email: "iets@mail.nl",
      phonenumber: "06557777",
      birthdate: "2020-01-01 10:10:00",
      gender: "Other",
    });
    user.save().catch((validationResult) => {
      const { message } = validationResult.errors.firstname;
      assert(message == "A user needs to have a firstname.");
      done();
    })
    .catch(done)
  });
});


